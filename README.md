# README #

The contents of this repo are the outcome  of  the ESA  VIBeS  project,  ArtesAT contract n. 4000122991/18/UK/ND [1]. 
The responsibility for the presented content resides with the authors.

In specific, this repository is used to keep the latest Docker images, configuration and management scripts of the virtual PEP (vPEP) architecture. The Virtual Network Functions (VNFs) available herein, contain all the required (partially) configured software which can be instantiated, through an ETSI MANO framework, by passing the proper parameters related to e.g., point of attachment.

Currently, the management and orchestration of the functions is supported by a customized version of the OpenBaton (https://github.com/openbaton) ETSI-compliant MANO framework found in the nfvo folder of this repo. For the succesfull deployment of the associated NSDs (nsd folder), the OpenBaton Docker driver is required as the VNFs are based on Docker images and need a Docker engine environment installed on each PoP.

### Repository Organization ###

* Docker images containing the (vPEP) VNFs to be installed in the POP nodes of the testbed.
* A customized version of the Openbaton NFVO used in the project. 
* Support documentation to deploy and run the testbed
* Support scripts e.g., NSD files, MPTCP network setup.

### Overall architecture ###

![VIBeS testbed](figure/VIBeS-Testbed.png)  
*Figure 1 - VIBeS Testbed overall architecture*  

POP1 is the main container, where VMs and VNFs associated to the PEP acceleration are hosted.
POP2 is a WAN emulator, including terrestrial and satelite links.
POP3 runs the end-clients applications and WEB servers.
The Satellite link emulation is enabled through the OpenSand environment with a set of additional VMs: https://opensand.org/

#### Data Path ####

![data path](figure/data-path.png)  
*Figure 2 - VIBeS Testbed data path*  

The testbed is divided into two parts: _Local_ and _Remote_ side.
The communication starts at the 'Local Side'.  

Let's assume the case a client (_web/video/sensor_) is hosted on VM Client or Docker Clients (both in POP3). 
The next element in the path is a VNF in POP1 that can be the vPEP (e.g. a vPEP client represented by the VNF with red boxes in _Figure 1_) or another network element such as a L3 Router (VNF with purple box in _Figure 1_). 
The VNF can be reached through custom Docker networks (namely APN in _Figure 1_) available on all VMs in POP3. 
Most important is the Docker network subnetting to avoid the Docker daemons on different VMs will not assign the same IP address to different Docker containers (i.e. App in _Figure 3_).  
Multiple Docker networks are defined at POP3 level for each use-case (i.e. Web-broswsing - _UC1_, Web-streaming - _UC2_, Sensor Network - _UC3_) or eventual sub use-case (i.e. UC1.1, UC1.2, ..., UC1.N; UC2.1, UC2.2, ..., UC2.N; UC3.1, UC3.2, ..., UC3.N;). 
This approach is useful to identify a network service by the APN Docker network.  

The VNF can leverage on one or multiple backhaul Docker networks available at POP1 level. For the scope of the project, backhaul links are Terrestrial or Satellite.  
The POP2 represents the middle point of the Testbed and thus a turning point between _Local_ and _Remote_ side. 
The communication continues on the _Remote side_ reaching as first the counter-VNF on POP1 (e.g. the vPEP server) and last the local-server in the POP3. 
In case of communication with a remote-server, POP3 is in charge of prepare (i.e. NAT) the communication for the public Internet. 

#### OpenSand configuration ####

![opensand-profile](figure/os-settings.png)  
*Figure 3 - OpenSand resource configuration. A bandwidth of 11.25MHz provides a carrier rate of 40MBit/s in download for the Forward Link*  



### Virtualization approach ###

A first virtualization layer is implemented with KVM/QEMU, on each of the 3 POPs. This allows to create a set of VMs suitable to host the Docker-based VNFs.

![virtualization approach](figure/hybrid-docker.png)  
*Figure 4 - Hybrid multi-level virtualization approach*  

The key aspect is to enable a pair of VMs with a patch to the Linux kernel for MPTCP in POP1. In this way, all Docker VNFs launched within thiese VMs will inherit the support of MPTCP.

#### MPTCP VM Optimizations ####

Both VMs for MPTCP have a custom receive buffer, needed to speed up the subflow MPTCP connection over the satellite link. 
In fact, the default values paired with automatic receive buffer tuning (`net.ipv4.tcp_moderate_rcvbuf = 1`) will affect the 
cWnd growth in the connection initial stages. The following parameters were applied:

`net.ipv4.tcp_rmem = 4096 506380 6291456`


### vPEP Protocol Suite ###

For the project we have considered the __MPTCP__ stable release _v0.94.3_ with a Linux kernel ver. _4.14.105.mptcp_
available from the github branch [2].  

For the __QUIC__ scenario we rely on an application proxy _“GOST” (v2.6)_ available from [3] in combination with 
the implementation of __QUIC__ in _GoLang_ (_quic-go v0.6_) available at [4].

### Network Service Descriptors ###

The NSD is essentially a json file describing a Service Function Chain (SFC) in terms of virtual network elements (i.e. VNF Descriptor - vnfd) and virtual networks (Virtual Link Descriptor - vld) involved. 
A set of NSD is provided in the [nsd folder](https://bitbucket.org/fzamps/vpep/src/master/nsd/) of the project. 
The structure of an NSD is provided below. 
The _'vld'_ (_virtual link descriptor_) parameter represents the set of Docker networks involved in the SFC. 
Usually it contains only deployed Docker networks. 
In case a not deployed network is provided in the set, the Docker daemon will create a new local Docker network with the provided name: the new network is of the type 'bridge'.  

The _'vnfd'_ (_vnf descriptor_) parameter contains all the information of the single VNF. In particular:

* __name__ actually represents the name of the VNF instance (i.e. Docker container, e.g. 'quic-client');  
* __vendor__ represents the vendor of the VNF (e.g. 'NITEL-Consortium');  
* __version__ actually is used to communicate with OpenBaton that VNFs are managed by Docker;  
* __vm_image__ actually represents the Docker image that will be used to deploy the VNF (e.g. nitel/gost:latest);  
* __vimInstanceName__ actually represents the VM where the VNF will be deployed (e.g. 'QUIC/TCP-vPEP-Client'). The provided name refers to a POP previously set on OpenBaton;  
* __connection_point__ acually represents the set of Docker networks connected (i.e. the virtual interfaces) to the VNF (i.e. the Docker container). This represent a sub-set of the networks provided in the _'virtual_link'_ field;  
* __virtual_link__ actually represents the set of available Docker networks for the VNF. This set contains a sub-set (eventually the entire set) of the networks provided in _'vld'_;  
* __configurationParameters__ actually represents the set of environment variable that will be setup when the Docker container is started;  
* __endpoint__ refers to the VNF Manager to use (in the VIBeS prooject a custom vnfm is used).  

```javascript
{
"name": "NSD-name",
  "vendor": "COMPANY",
  "version": "docker",
  "vld": [...],
  "vnfd": [
  		{
         "vdu":[ 
            { 
               "vm_image":[...],
               "vimInstanceName":[...],
               "scale_in_out":1,
               "vnfc":[{"connection_point":[...]}]
            }],
         "virtual_link":[...],
         "configurations":{ 
            "name":"ems",
            "configurationParameters":[...]
         },
         "type":"proxy",
         "endpoint":"vnfmDocker"
      	},
    {...},
    {...}
  ]
}
```


### External links ###
* [1] Project offical web page: https://artes.esa.int/projects/vibes
* [2] https://github.com/multipath-tcp/mptcp/tree/v0.94.3
* [3] https://github.com/ginuerzh/gost/tree/v2.6
* [4] https://github.com/lucas-clemente/quic-go/tree/v0.6.0

### Contancts ###

* Francesco Zampognaro: zampognaro@ing.uniroma2.it
* Armir Bujari: abujari@math.unipd.it
