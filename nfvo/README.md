
# Installation of the NFVO

This repository contains a customized version of the Openbaton NFVO trailored to the needs of the ESA ARTES VIBeS project 
(https://artes.esa.int/projects/vibes). Main modifications include but are not limited to: (i) introduction to new API calls used by a higher level orchestrator used during service design (ii) a lightweight way to solve functional dependencies between chained VNFs (part of the NSD) and (ii) a more robust error handling scheme during the deployment phase. 


```javascript
sh <(curl -H 'Cache-Control: no-cache' -s <dir>/bootstrap.sh develop --config-file=<abs--config-file-path>
```
A sample configuration file for the installation can be found under the install diretory (bootstrap-config-file) along with the bootstrap.sh installation script.





